﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileInfoToolbox
{
    public class FileInfoToolbox
    {

        // Static fields
        public static string[] _FileTypeGroup;

        public enum FileTypeGroup
        {
            MediaAll,
            MediaAudio,
            MediaVideo,
            MediaImages,
            Office
        };

        // public methods
        public static async Task<List<FileInfo>> GetFilesByFileTypeGroup(string folder, FileTypeGroup fileGroup, bool recursive)
        {
            string[] extensions;
            List<FileInfo> mFileInfos = new List<FileInfo>();

            try
            {

                // Set extension to scan for.
                switch (fileGroup)
                {
                    case FileTypeGroup.MediaAll:
                        extensions = new[] { ".mp4", ".mpv", ".avi", ".mkv", ".mp3", ".wav", ".ogg", ".png", ".jpg", ".tiff" };
                        break;

                    case FileTypeGroup.MediaAudio:
                        extensions = new[] { ".mp3", ".wav", ".ogg" };
                        break;

                    case FileTypeGroup.MediaImages:
                        extensions = new[] { ".png", ".jpg", ".tiff" };
                        break;

                    case FileTypeGroup.MediaVideo:
                        extensions = new[] { ".mp4", ".mpv", ".avi", ".mkv" };
                        break;
                    case FileTypeGroup.Office:
                        extensions = new[] { ".xls", ".xlsx", ".doc", ".docx" };
                        break;

                    default:
                        extensions = new[] { "" };
                        break;
                }

                // Set folder to scan.
                DirectoryInfo dirInfo = new DirectoryInfo(folder);

                // Scan for files.
                if (recursive)
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories)
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }
                else
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*")
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }


                return mFileInfos;
            }
            catch
            {
                return mFileInfos;
            }

        }

        public async Task<List<FileInfo>> GetFilesByExtensionList(string folder, string[] extensions, bool recursive)
        {

            List<FileInfo> mFileInfos = new List<FileInfo>();

            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(folder);

                if (recursive)
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*", SearchOption.AllDirectories)
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }
                else
                {
                    mFileInfos = dirInfo.EnumerateFiles("*.*")
                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                        .ToList<FileInfo>();
                }


                return mFileInfos;
            }
            catch
            {
                return mFileInfos;
            }

        }

    }

}
